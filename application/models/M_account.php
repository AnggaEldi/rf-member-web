<?php 

    class M_account extends CI_Model {
        public $staff_table = 'dbo.tbl_StaffAccount';
        public $user_table  = 'dbo.tbl_RFTestAccount';
        public $regist_reward = true;
        public $rewards = array('coin_amount' => 10000, 'premium_service_day' => 7 );
        public function __construct()
        {
                // Call the CI_Model constructor
                $this->load->database();
                $this->load->library('Event_lib');
                parent::__construct();
        }

        public function get_staff_accounts()
        {
            $query = $this->db->get( $this -> user_table );
            return $query->result();
        }

        public function register_account($_username, $_password) {
            $regist_status = false;
            $_clean_user = $this->db->escape($_username);
            $_clean_pwd  = $this->db->escape($_password);
            if( $this -> user_not_exist($_username) ) {
                $insert  = $this->insert_user($_clean_user, $_clean_pwd);
                if( $insert ){
                    $regist_status = true;
                    $this -> insert_reward( $_username );
                }
            } 
            return $regist_status;
        }

        private function user_not_exist($user_id){
            $exist_user = true;
            $user_count = " SELECT id
                            FROM   ".$this -> user_table."
                            WHERE  id = CONVERT(binary, '".$user_id."' ) ";
                $count  = $this -> db -> query($user_count);
            if( $count->num_rows() >= 1 ){
                $exist_user = false;
            } 
            return $exist_user;
        }

        private function insert_user($_username, $_password ){
            $is_insert = false;
            $regist    = " INSERT INTO ".$this->user_table." ( id, password ) 
                          VALUES ( CONVERT(binary, {$_username} ),
                                   CONVERT(binary, {$_password} ) ) ";
            return $this->db->query($regist);
        }

        private function insert_reward($_userid){
            $success_query    = false;
            $BILLING_DB       = $this->load->database('billing', TRUE);
            $event            = $this->event_lib->get_detail_event('newbie-rewards');
            $_reward          = $event['parameter'];
            $_coin_amount     = $_reward['coins'];
            $_premium_amount  = $_reward['premium_days'];
            $_clean_uid       = $this->db->escape($_userid);
            $premi_start_date = date('Y-m-d');
            $premi_end_date   = date('Y-m-d', strtotime($premi_start_date.' + '.$_premium_amount.' days')); 
            // print_r($premi_start_date);
            $reward_query    = " INSERT INTO tbl_UserStatus
                                 ( id, status, DTStartPrem, DTEndPrem, Cash ) 
                                 VALUES ( {$_clean_uid}, '2', '{$premi_start_date}', '{$premi_end_date}', {$_coin_amount} ) ";
            if( $BILLING_DB->query($reward_query) )
            {
                $success_query = true;
            }
            return $success_query;
        }
    }

?>