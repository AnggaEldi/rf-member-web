<?php 
    class General_lib
	{
	 
		function __construct()
		{
		}

		function generate_number(){
			$min = 10000;
			$max = 99999;
			$rand = rand($min,$max);
			return $rand;
		}

		function generate_string($length = 7) {
			return substr(str_shuffle(str_repeat($x='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
		}

		function itemdb_convert($input){
			$dechex = dechex($input);

			$item_id_pos 	= strlen($dechex) - 4;
			$item_id_len 	= substr($dechex, 0, $item_id_pos);
			$item['id'] 	= hexdec($item_id_len);

			$item_type_pos 	= strlen($dechex) - strlen($item_id_len) - 2;
			$item_type_len 	= substr($dechex, $item_id_pos, $item_type_pos);
			$item['type']  	= hexdec($item_type_len);

			$item_slot_pos 	= strlen($dechex) - strlen($item_id_len) - strlen($item_type_len);
			$item_slot_len 	= substr($dechex, $item_id_pos + $item_type_pos, $item_slot_pos);
			$item['slot'] 	= hexdec($item_slot_len);

			return $item;
		}

		function sql_convert($input){
			return 65536 * $input + ($type * 256 + $slot);
		}
		
	}
?>
