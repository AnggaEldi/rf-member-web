<?php 
    class Event_lib
	{
	 
		function __construct()
		{
		}

		function get_event_list(){
			/**
			 * Event list coded by fullwar
			 * 28 July 2020
 			 */
			$EVENTS = array(
				'pvp-exchanges' => array(
					'is_active' 	=>  true, #toggle true/false 
					'is_claimable'  =>  true,
					'url'  			=> 'pvp-exchanges',
					'view'    		=> 'event_exchange', #link to function controller, leave if yo dont know what r u doin
					'title' 		=> 'Exchange pvp to coins',
					'description' 	=> 'Exchange pvp to coins',
					'thumbnail' 	=> 'assets/image/event/pvp-coins.png', #64x64 images
					'parameter' 	=>  array( 'rate_pvp_to_coins' => 2 ) #it means 1 pvp will get 2 coins 
				),
				'newbie-rewards' => array(
					/**
					 * this event will be activated automatically 
					 * if variable 'is_active' set to true
					 */
					'is_active' 	=>  true,
					'is_claimable'  =>  false,
					'url'  			=>  'newbie-rewards',
					'view'   		=>  NULL, 
					'thumbnail' 	=> 'assets/image/event/pvp-coins.png', 
					'title' 		=> 'Newbie rewards',
					'description' 	=> 'New registered player get premium services and coin',
					'parameter' 	=>  array( 'premium_days' => 7, 'coins' => 50000 ) #registered player will get 7 days premium, and 50k coins
				)
			);
			return $EVENTS;
		}

		function get_detail_event($event_code){
			$event_list 	= $this->get_event_list();
			$event_detail 	= false;
			if( array_key_exists($event_code, $event_list) ){
				$event_detail = $event_list[$event_code];
			}
			return $event_detail;
		}
	}
?>
