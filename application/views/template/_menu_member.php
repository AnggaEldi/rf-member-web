   <!-- left menu area -->
   <div class="col-xs-3">
              <div class="list-group">
                <a  class="list-group-item disabled">
                  Payment and event
                </a>
                <a href="<?php echo site_url('payment/premium_service'); ?>" 
                   class="list-group-item <?php if( isset($page_ctl) &&  $page_ctl == 'premium_service' ) echo 'active';?>">Premium Services</a>
                <a href="<?php echo site_url('payment/coin_topup'); ?>" 
                   class="list-group-item <?php if( isset($page_ctl) &&  $page_ctl == 'coin_topup' ) echo 'active';?>">Ingame Coin</a>
                <a href="<?php echo site_url('payment'); ?>" 
                   class="list-group-item <?php if( isset($page_ctl) &&  $page_ctl == 'event_list' ) echo 'active';?> ">Event List</a>
              </div>
              <div class="list-group">
                <a  class="list-group-item disabled">
                  Member Menu
                </a>
                <a href="<?php echo site_url('member/change_password'); ?>" 
                   class="list-group-item <?php if( isset($page_ctl) && $page_ctl == 'change_password' ) echo 'active';?>">Change Password</a>
                <a href="<?php echo site_url('member/restore_bank'); ?>" 
                    class="list-group-item <?php if( isset($page_ctl) &&  $page_ctl == 'restore_bank' ) echo 'active';?>">Restore Bank Password</a>
                <!-- <a href="<?php echo site_url('member/restore_guard'); ?>" 
                    class="list-group-item <?php if( isset($page_ctl) &&  $page_ctl == 'restore_guard' ) echo 'active';?>">Restore Fireguard</a> -->
                <a href="<?php echo site_url('authentication/do_logout'); ?>" class="list-group-item">Logout</a>
              </div>
              
            </div>
            <!-- .left menu area -->