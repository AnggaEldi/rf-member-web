<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
 	<?php include('template/_header_member.php');  ?>
	<!-- editable section -->
    <div id="spacer" style="padding-top:5em;"></div>
    <div class="container theme-showcase" role="main">
      <!-- Main jumbotron for a primary marketing message or call to action -->

      <div class="container">
        <div class="row">
            <!-- header area -->
            <div class="col-xs-12">
              <div class="page-header">
                <h1><?php echo $page_title; ?> <small></small></h1>
              </div>
              <div class="breadcrumb">
                <?php foreach($breadcrumb as $bc) :
                        $link = site_url($bc['link']);
                        echo "<li> <a href='{$link}' class='{$bc['classes']}'> {$bc['label']} </a> </li>";
                      endforeach; ?>
                <div class="pull-right"> server status : <b><a href="#" style="color: green" >online</a></b> </div>
              </div>
            </div>
            <!-- ./header area -->
            <!-- left menu area -->
            <?php include('template/_menu_member.php') ?>
            <!-- .left menu area -->

            <div class="col-md-9 col-xs-12">
              <!-- right area -->
              <div class="row">
                <!-- topup thumbnail -->
                <div class="col-xs-12 col-md-6">
                  <div class="panel panel-primary"> 
                    <div class="panel-body products" style="background-color:#337ab7">
                      <div class="col-xs-4">
                        <div class="thumbnail"></div>
                      </div>
                      <div class="col-xs-8">
                        <h3 class="title">
                          Premium Services
                        </h3>
                        <p> Get 2x exp, loots, more pvp point.</p>
                        <a class="btn btn-success btn-block">
                          <i class="glyphicon glyphicon-upload"></i>
                          Buy premium services
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="col-xs-12 col-md-6">
                  <div class="panel panel-danger"> 
                    <div class="panel-body products" style="background-color:#1abc9c">
                      <div class="col-xs-4">
                        <div class="thumbnail2"></div>
                      </div>
                      <div class="col-xs-8">
                        <h3 class="title">
                          Ingame Coins
                        </h3>
                        <p> Buy exclusive cashshop with coins.</p>
                        <a class="btn btn-success btn-block">
                          <i class="glyphicon glyphicon-shopping-cart"></i>
                          Topup coins
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- ./topup thumbnail -->
                
                <!-- event list -->
                <div class="col-xs-12">
                  <div class="page-header">
                    <h3>Event List</h3>
                  </div>
                  <!-- <pre><?php print_r($event_list);?></pre> -->
                  <?php foreach($event_list as $event) : ?>
                    <div class="col-sm-6 col-md-4"> 
                      <div class="thumbnail"> 
                          <img alt="100%x200" 
                               src="assets"
                               style="height: 200px; width: 100%; display: block;" > 
                        <div class="caption"> 
                          <h4><?php echo $event['title'] ?></h4> 
                            <p><?php echo $event['description'] ?></p> 
                            <?php if($event['is_claimable']): ?>
                              <!-- <a href="<?php  echo site_url('event/'.$event['url']) ?>" class="btn btn-primary btn-block" role="button" >Claim event</a>  -->
                              <button class="btn btn-primary btn-block" onClick="alert('coming soon')">claim</button>
                            <?php endif; ?>
                        </div>
                      </div> 
                    </div>
                  <?php endforeach; ?>
                <!-- ./event list -->
                <div class="spacer"></div>
                <div class="spacer"></div>
              </div>
              <!-- .right area -->
        </div>
      </div>

    </div>
	<!-- ./ editable section -->
 	<?php include('template/_footer_member.php');  ?>
</html>
