<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	private $currentLoginUser;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_member');
		$this->load->library('session');
		$this->load->library('event_lib');
		if ( !$this->session->has_userdata('account') ){
			redirect('login', 'refresh');
		}
		$_account =  $this->session->userdata('account');
		$this->currentLoginUserId = $_account->id;
	}

	public function index()
	{
		$data['page_title'] = "Payment and event";
		$data['breadcrumb'] = array( array( 'link' => 'member', 
											'label' =>'Member Area', 
											'classes' => '' ), 
									 array( 'link' => 'member/payment-event', 
											'label' =>'Payment and event', 
											'classes' => 'active' )
									);
		$data['page_ctl']   = 'event_list';
		$data['account'] 	= $this->session->userdata('account');
		$data['event_list'] = $this->event_lib->get_event_list(); #check libraries/Event_lib.php to change event
		$this->load->view('Payment_home', $data); 
	}

	public function coin_topup()
	{
		$data['page_title'] = "Topup coin";
		$data['breadcrumb'] = array( array( 'link' => 'member', 
											'label' =>'Member Area', 
											'classes' => '' ), 
									 array( 'link' => 'member/coin_topup', 
											'label' =>'Topup coin', 
											'classes' => 'active' )
									);
		$data['page_ctl']   = $this->uri->segment(2);
		$data['account'] 	= $this->session->userdata('account');
		$this->load->view('coming_soon', $data); 
	}

	public function premium_service()
	{
		$data['page_title'] = "Premium service";
		$data['breadcrumb'] = array( array( 'link' => 'member', 
											'label' =>'Member Area', 
											'classes' => '' ), 
									 array( 'link' => 'member/premium_service', 
											'label' =>'Premium service', 
											'classes' => 'active' )
									);
		$data['page_ctl']   = $this->uri->segment(2);
		$data['account'] 	= $this->session->userdata('account');
		$this->load->view('coming_soon', $data); 
	}

}
