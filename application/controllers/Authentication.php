<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function index()
	{
		if( !isset($_POST['submit']) ){
			$this->load->view('login_form');
		} else {
			if($this->do_login()){
				#check is user session hasbeen setup
				redirect('member', 'refresh');
			} else {
				$this->session->set_flashdata('login_fail', true);
				$this->load->view('login_form');
			}
		}
	}
	

	private function do_login(){
		$is_login = false;
		if( !empty($_POST['username']) && !empty($_POST['password'])  ){
			$username = $this -> input -> post('username');
			$password = $this -> input -> post('password');
			$this->load->model('M_login');
			$login = $this->M_login->login_account($username,$password);
			if( $login ) $is_login = true;
		}
		return $is_login;
	}

	public function do_logout(){
		$this->session->unset_userdata('account');
		$this->session->set_flashdata('logged_out',true);
		redirect('login', 'refresh');
	}

}
