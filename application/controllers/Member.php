<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
	private $currentLoginUser;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_member');
		$this->load->library('session');
		$this->load->library('character_lib');
		$this->load->library('general_lib');
		if ( !$this->session->has_userdata('account') ){
			redirect('login', 'refresh');
		}
		$_uid =  $this->session->userdata('account');
		$this->currentLoginUserId = $_uid->id;
		
	}

	public function index()
	{
		$data['page_title'] = "Dashboard";
		$data['breadcrumb'] = array( array( 'link' => 'member', 
											'label' =>'Member Area', 
											'classes' => '' ), 
									 array( 'link' => 'member', 
											'label' =>'dashboard', 
											'classes' => 'active' )
									);
		$_characters = $this->get_character($this->currentLoginUserId) -> result_object();
		$_rebuild    = $this->character_lib->rebuild_character($_characters);
		$data['characters'] = $_rebuild;
		$data['account'] 	= $this->session->userdata('account');
		$this->load->view('member_home', $data);
	}

	private function get_character($userid){
		return $this->M_member->find_character($userid);
	}

	public function change_password(){
		$data['page_title'] = "Change password";
		$data['page_ctl'] 	= $this->uri->segment(2);
		$data['breadcrumb'] = array( array( 'link' => 'member', 
											'label' =>'Member Area', 
											'classes' => '' ), 
									 array( 'link' => 'member/change_password', 
											'label' =>'Change password', 
											'classes' => 'active' )
									);
		$data['account'] 	= $this->session->userdata('account');
		
		if(isset($_POST['submit'])){
			$current_pass = $this->input->post('current_password');
			$newpass  	  = $this->input->post('new_password');
			$newpass2 	  = $this->input->post('new_password2');

			if( $current_pass != '' && $newpass !='' && $newpass2 !='' ){
				if( $newpass != $newpass2 ){
					$this->session->set_flashdata('submit_error', 'Retype password is different');
				} else {
					$_uid = $this->currentLoginUserId;
					$this->load->model('M_login');
					$find_user = $this->M_login->find_user($_uid, $current_pass);
					if($find_user->num_rows() < 1){
						$this->session->set_flashdata('submit_error', 'Current password didnt match');
					} else {
						$set_pass = $this->M_member->change_password($_uid, $newpass);
						if($set_pass) $this->session->set_flashdata('submit_success','Password has been changed.');
					}
				}
			} else {
				$this->session->set_flashdata('submit_error','Field is required');
			}
		}

		$data['submit_error'] 	= $this->session->flashdata('submit_error');
		$data['submit_success']	= $this->session->flashdata('submit_success');	
		$this->load->view('member_changepwd', $data);
	}

	public function restore_bank(){
		$data['page_title'] = "Restore bank";
		$data['page_ctl'] 	= $this->uri->segment(2);
		$data['breadcrumb'] = array( array( 'link' => 'member', 
											'label' =>'Member Area', 
											'classes' => '' ), 
									 array( 'link' => 'member/restore_bank', 
											'label' =>'Restore Bank Password', 
											'classes' => 'active' )
									);
		$data['account'] 	= $this->session->userdata('account');
		
		if(isset($_POST['submit'])){
			$password = $this->input->post('current_password');
			$is_agree = $this->input->post('agree');
			$_uid 	  = $this->currentLoginUserId;
			
			if($password != '' && $is_agree !=''){
				$_uid = $this->currentLoginUserId;
				$this->load->model('M_login');
				$find_user = $this->M_login->find_user($_uid, $password);
				if($find_user->num_rows() < 1){
					$this->session->set_flashdata('submit_error', 'Current password didnt match');
				} else {
					$random_number 	= (string)$this->general_lib->generate_number();
					$account_serial = $this->M_member->get_account_serial($_uid);
					if($account_serial == null){
						$this->session->set_flashdata('submit_error', 'You didnt have any characters, please create one.');
					} else {
						$check_trunk 	= $this->M_member->get_account_trunk($account_serial);
						$user_trunk  	= $check_trunk -> result_object();
						if($user_trunk[0]->HintIndex == 255 && $user_trunk[0]->HintAnswer == '*'  ){
							$this->session->set_flashdata('submit_error', 'Your bank account is not available, please create one in game');
						} else {
							$change_trunk_pass = $this->M_member->set_account_trunk($account_serial, $random_number);
							if($change_trunk_pass){
								$message = "Your bank account password is set to <b>".$random_number."</b>";
								$this->session->set_flashdata('submit_success', $message);
							}
						}
					}
				}
			}
		}
		$data['submit_error'] 	= $this->session->flashdata('submit_error');
		$data['submit_success']	= $this->session->flashdata('submit_success');	
		$this->load->view('member_restorebank', $data);
		
	}

	public function restore_guard(){
		$data['page_title'] = "Change password";
		$data['page_ctl'] 	= $this->uri->segment(2);
		$data['breadcrumb'] = array( array( 'link' => 'member', 
											'label' =>'Member Area', 
											'classes' => '' ), 
									 array( 'link' => 'member/restore_guard', 
											'label' =>'Restore Bank Password', 
											'classes' => 'active' )
									);
		$data['account'] 	= $this->session->userdata('account');
		echo $this->general_lib->generate_string();
		if(isset($_POST['submit'])){
			$password = $this->input->post('current_password');
			$is_agree = $this->input->post('agree');
			$_uid 	  = $this->currentLoginUserId;
			
			if($password != '' && $is_agree !=''){
				$_uid = $this->currentLoginUserId;
				$this->load->model('M_login');
				$find_user = $this->M_login->find_user($_uid, $password);
				if($find_user->num_rows() < 1){
					$this->session->set_flashdata('submit_error', 'Current password didnt match');
				} else {
					$random_number 	= (string)$this->general_lib->generate_number();
					$account_serial = (string)$this->M_member->get_account_serial($_uid);
					if($account_serial == null){
						$this->session->set_flashdata('submit_error', 'Your fireguard is not setup yet, please setup in game.');
					} else {
						$do_change_fg 	= $this->M_member->change_fg($account_serial, $random_number);
						if($do_change_fg) {
							$message = "Your fireguard password is set to <b>".$random_number."</b>";
							$this->session->set_flashdata('submit_success', $message);
						}
					}
				}
			}
		}
		$data['submit_error'] 	= $this->session->flashdata('submit_error');
		$data['submit_success']	= $this->session->flashdata('submit_success');	
		$this->load->view('member_restorefg', $data);

	}


}
