<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_account');
		$this->load->library('session');

	}

	public function index()
	{
			#check if set post
			if(isset($_POST['submit'])){
				$_username = $this->input->post('username');
				$_password = $this->input->post('password');
				$register  = $this->insert_user($_username, $_password);
				if( $register ){
					#success register
					$this->session->set_flashdata('register_status', true);
					redirect('login','refresh');
				} else {
					$data['title'] = 'Register account';
					$this->session->set_flashdata('fail_register', true);
					$this->load->view('register_account', $data);
				}
				
			} else {
				#account view	
				$data['title'] = 'Register account';
				$this->load->view('register_account', $data);
			}
	}


	private function insert_user($_username, $_password){
		return  $this -> M_account -> register_account($_username, $_password);
	}
}
