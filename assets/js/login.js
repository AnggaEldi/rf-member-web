
document.getElementById('inputEmail')
        .addEventListener("blur", (ev) =>{
            let username = ev.target.value;
            var submit   = document.getElementById('submit');
            var help     = document.getElementById('uname_help');
            if(username.length >= 0){
                var regtxt = /^[a-z][a-z0-9]{3,12}$/;
                if(!regtxt.test(username)){
					//check format email
                    submit.setAttribute("disabled",true);
                    help.classList.remove('hidden');
                }else{
					//else show the cheking_text and run the function to check
                    submit.removeAttribute("disabled");
                    help.classList.add('hidden');

				}
            }
        })